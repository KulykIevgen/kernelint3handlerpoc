﻿#include "pch.h"
#include <iostream>
#include <Windows.h>

int main()
{
	BOOL result;

	__asm {
		nop
	};
	if (CheckRemoteDebuggerPresent(GetCurrentProcess(), &result))
	{
		if (result)
			std::cout << "Debugger detected" << std::endl;
		else
			std::cout << "Debugger not found" << std::endl;
	}
	else
		std::cout << "Error" << std::endl;
	return 0;
}