extern "C"
{
#include <Ntifs.h>
}
#include "Shared.h"
#include "Idt.h"

#define SYMLINK_NAME L"\\DosDevices\\DebugDevice"
#define DEVICE_NAME L"\\Device\\DebugDevice"

#define BreakSize 100
Breakpoint breaks[BreakSize]{ 0 };
unsigned int counter{ 0 };

extern "C" VOID OnUnload(IN PDRIVER_OBJECT DriverObject)
{
	UNICODE_STRING SymLink = RTL_CONSTANT_STRING(SYMLINK_NAME);
	IoDeleteSymbolicLink(&SymLink);
	IoDeleteDevice(DriverObject->DeviceObject);	
}

extern "C" NTSTATUS Default_Handler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{	
	(void)DeviceObject;
	Irp->IoStatus.Information = 0;
	Irp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

extern "C" NTSTATUS DeviceControlHandler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	ULONG inBufLength{ 0 };
	ULONG outBufLength{ 0 };
	PBreakpoint inBuf{ nullptr };
	NTSTATUS result{ STATUS_UNSUCCESSFUL };

	UNREFERENCED_PARAMETER(DeviceObject);
	PIO_STACK_LOCATION irpSp = IoGetCurrentIrpStackLocation(Irp);
	if (!irpSp)
	{
		Irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return STATUS_INVALID_PARAMETER;
	}

	inBufLength = irpSp->Parameters.DeviceIoControl.InputBufferLength;
	outBufLength = irpSp->Parameters.DeviceIoControl.OutputBufferLength;

	switch (irpSp->Parameters.DeviceIoControl.IoControlCode)
	{
	case IOCTL_DEBUGGER_HOOK_IDT:
		{
			Interrupts in;
		}
		result = STATUS_SUCCESS;
		break;
	case IOCTL_DEBUGGER_INSERT_BREAKPOINT:
		inBuf = reinterpret_cast<PBreakpoint>(Irp->AssociatedIrp.SystemBuffer);
		if (inBufLength == sizeof(Breakpoint) && counter < BreakSize)
		{
			breaks[counter++] = *inBuf;
			result = STATUS_SUCCESS;
		}
		else
		{
			result = STATUS_INVALID_PARAMETER;
		}		
		break;
	default:
		break;
	}

	Irp->IoStatus.Status = result;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return result;
}


extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT pDriverObject,
	PUNICODE_STRING pRegistryPath)
{
	(void)pRegistryPath;
	PDEVICE_OBJECT TheDevice = NULL;
	pDriverObject->DriverUnload = OnUnload;
	UNICODE_STRING DeviceName = RTL_CONSTANT_STRING(DEVICE_NAME);
	NTSTATUS status = IoCreateDevice(pDriverObject, 0, &DeviceName, FILE_DEVICE_UNKNOWN,
		FILE_DEVICE_SECURE_OPEN, FALSE, &TheDevice);

	if (NT_SUCCESS(status))
	{
		for (unsigned int i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; i++)
		{
			pDriverObject->MajorFunction[IRP_MJ_CREATE] =
				pDriverObject->MajorFunction[IRP_MJ_CLOSE] = Default_Handler;
			pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DeviceControlHandler;
		}

		TheDevice->Flags |= DO_BUFFERED_IO;

		UNICODE_STRING SymLink = RTL_CONSTANT_STRING(SYMLINK_NAME);
		status = IoCreateSymbolicLink(&SymLink, &DeviceName);
	}

	return status;
}