#include "Idt.h"
#include "Guard.h"

#define BreakSize 100
extern Breakpoint breaks[BreakSize];
extern unsigned int counter;

DWORD OriginalAddress = 0;

DWORD __stdcall SearchBreakpoint(DWORD Address)
{
	DWORD realAddress = Address - 1;
	for (unsigned int i = 0; i < counter; i++)
	{
		DWORD current = breaks[i].address;
		if (current == realAddress)
			return 1;
	}
	return 0;
}

void __stdcall dump()
{
	
}

__declspec(naked) void KiTrapHandler()
{
	__asm {
		mov eax, dword ptr [esp]
		push eax
		call SearchBreakpoint
		cmp eax,1
		je handle		
		jmp OriginalAddress
	handle:
		call dump
		iretd
	}
}

PIDTDescr Interrupts::GetIDTAddress() const
{	
	IDTR container = { 0 };

	__asm {
		cli
		sidt container
		sti
	}

	return reinterpret_cast<PIDTDescr>( (static_cast<DWORD>( container.baseAddressHigh ) << 16) 
		+ container.baseAddressLow );
}

DWORD Interrupts::MakeAddress(WORD lowpart, WORD highpart) const
{
	return (static_cast<DWORD>(highpart) << 16) + lowpart;
}

Interrupts::Interrupts()
{
	disableWriteProtection();
	PIDTDescr begin = GetIDTAddress();
	if (begin)
	{
		auto* KiTrap03Descriptor = &begin[3];
		OriginalAddress = MakeAddress(KiTrap03Descriptor->offset_1, KiTrap03Descriptor->offset_2);
		auto address = KiTrapHandler;
		KiTrap03Descriptor->offset_1 = static_cast<WORD>(reinterpret_cast<DWORD>(address) & 0x0000FFFF);
		KiTrap03Descriptor->offset_2 = static_cast<WORD>( (reinterpret_cast<DWORD>(address) & 0xFFFF0000) >> 16 );
	}
}

Interrupts::~Interrupts()
{

	enableWriteProtection();
}

void Interrupts::disableWriteProtection() const
{
	__asm {
		push eax
		mov eax, CR0
		and eax, 0xFFFEFFFF
		mov CR0, eax
		pop eax
	}
}

void Interrupts::enableWriteProtection() const
{
	__asm {
		push eax
		mov eax, CR0
		or eax, 0x00010000
		mov CR0, eax
		pop eax
	}
}
