#pragma once
extern "C"
{
#include <ntifs.h>
}

class lock_fast_mutex
{
public:
	lock_fast_mutex(PFAST_MUTEX mutex):
		m_mutex(mutex)
	{
		ExAcquireFastMutex(m_mutex);
	}
	~lock_fast_mutex()
	{
		ExReleaseFastMutex(m_mutex);
		m_mutex = nullptr;
	}

private:
	PFAST_MUTEX m_mutex;
};