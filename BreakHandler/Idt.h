#pragma once
extern "C"
{
#include <ntifs.h>
}

#include "Guard.h"

using WORD = unsigned short;
using DWORD = unsigned long;
using BYTE = unsigned char;
using uint16_t = WORD;
using uint8_t = BYTE;

#pragma pack(1)
typedef struct _IDTR
{
	WORD nBytes;
	WORD baseAddressLow;
	WORD baseAddressHigh;
}IDTR, *PIDTR;

typedef struct _IDTDescr
{
	uint16_t offset_1; // offset bits 0..15
	uint16_t selector; // a code segment selector in GDT or LDT
	uint8_t zero;      // unused, set to 0
	uint8_t type_attr; // type and attributes, see below
	uint16_t offset_2; // offset bits 16..31
}IDTDescr, *PIDTDescr;

enum Action
{
	Dump,
	Skip
};

typedef struct _Breakpoint
{
	DWORD address;
	DWORD processId;
	Action action;
}Breakpoint,*PBreakpoint;

#pragma pack()

class Interrupts
{
public:
	Interrupts();
	Interrupts(const Interrupts&) = delete;
	Interrupts(Interrupts&&) = delete;
	~Interrupts();

private:
	void disableWriteProtection() const;
	void enableWriteProtection() const;
	PIDTDescr GetIDTAddress() const;
	DWORD MakeAddress(WORD lowpart, WORD highpart) const;
};
