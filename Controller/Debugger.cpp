#include "pch.h"
#include "Debugger.h"
#include "ErrorInfo.h"
#include "Shared.h"
#include <iostream>

Debugger::Debugger():
	DebuggerDevice(INVALID_HANDLE_VALUE)
{
	auto driverFullPath = Debugger::GetCurrentPath() + "\\";
	driverFullPath += DriverFileName;
	Installer = std::make_unique<DriverInstaller>(driverFullPath);
	Installer->Start();
	DebuggerDevice = CreateFileA(DeviceName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
		nullptr, OPEN_EXISTING, 0, 0);
}


Debugger::~Debugger() noexcept
{	
	if (DebuggerDevice != INVALID_HANDLE_VALUE)
	{
		CloseHandle(DebuggerDevice);
	}
	Installer->Stop();
	Installer->Delete();
}

ProcessInfo Debugger::MakeAffinityProcess(const std::string& ExeFullPath) const noexcept
{
	STARTUPINFOA StartInfo { 0 };
	PROCESS_INFORMATION ProcInfo { 0 };
	ProcessInfo output { 0 };

	auto result = CreateProcessA(ExeFullPath.c_str(), nullptr, nullptr, nullptr,
		FALSE, CREATE_SUSPENDED, nullptr, nullptr, &StartInfo, &ProcInfo);

	if (result)
	{
		output.ProcessHandle = ProcInfo.hProcess;
		output.ProcessId = ProcInfo.dwProcessId;
		output.ThreadHandle = ProcInfo.hThread;

		DWORD_PTR ProcessMask, SystemMask;
		if (GetProcessAffinityMask(output.ProcessHandle, &ProcessMask, &SystemMask))
		{
			output.Affinity = ProcessMask;
		}
	}

	return output;
}

void Debugger::HookIdt(DWORD_PTR Affinity) const
{
	std::cout << "Set affinity mask for the current process" << std::endl;
	if (!SetProcessAffinityMask(GetCurrentProcess(), Affinity))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "Hooking idt for all processes" << std::endl;
	for (uint32_t threadAffinity = 1U; threadAffinity <= static_cast<uint32_t>(Affinity); threadAffinity <<= 1)
	{
		std::cout << "Current thread affinity is " << std::hex << threadAffinity << std::dec << std::endl;
		HookIdtForProcessor(threadAffinity);
	}
}

uint8_t Debugger::SetBreak(uint32_t Address, const ProcessInfo& information) const
{
	uint8_t oldbyte{ 0 };
	SIZE_T processed_bytes{ 0 };
	const uint8_t BreakOpcode{ 0xCC };
	DWORD oldProtect{ 0 };

	std::cout << "Starting set breakpoint" << std::endl;
	std::cout << "VirtualProtectEx on break address" << std::endl;
	if (!VirtualProtectEx(information.ProcessHandle, reinterpret_cast<LPVOID>(Address), 
		sizeof(oldbyte), PAGE_EXECUTE_READWRITE, &oldProtect))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "reading memory" << std::endl;
	if (!ReadProcessMemory(information.ProcessHandle, reinterpret_cast<LPCVOID>(Address), &oldbyte, sizeof(oldbyte), &processed_bytes))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "writing breakpoint" << std::endl;
	if (!WriteProcessMemory(information.ProcessHandle, reinterpret_cast<LPVOID>(Address), &BreakOpcode, sizeof(BreakOpcode), &processed_bytes))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "Flushing cache" << std::endl;
	if (!FlushInstructionCache(information.ProcessHandle, reinterpret_cast<LPVOID>(Address), sizeof(oldbyte)))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "restoring protection" << std::endl;
	if (!VirtualProtectEx(information.ProcessHandle, reinterpret_cast<LPVOID>(Address),
		sizeof(oldbyte), oldProtect, &oldProtect))
	{
		throw ErrorInfo::MakeLastError();
	}

	Breakpoint breakpoint{static_cast<DWORD>(Address), information.ProcessId, Action::Dump};
	std::cout << "insert breakpoint to kernel" << std::endl;
	if (!DeviceIoControl(DebuggerDevice, IOCTL_DEBUGGER_INSERT_BREAKPOINT, &breakpoint,
		sizeof(breakpoint), nullptr, 0, &processed_bytes, nullptr))
	{
		throw ErrorInfo::MakeLastError();
	}
	std::cout << "Finish the execution" << std::endl;
	return oldbyte;
}

void Debugger::HookIdtInCurrentContext() const noexcept
{
	DWORD ReturnBytes{ 0 };
	std::cout << "Call DeviceIoControl to hook idt" << std::endl;
	auto result = DeviceIoControl(DebuggerDevice, IOCTL_DEBUGGER_HOOK_IDT, nullptr, 0, nullptr, 0, &ReturnBytes, nullptr);
	std::cout << "Leave DeviceIoControl to hook idt, result: " << result << std::endl;
}

void Debugger::HookIdtForProcessor(uint32_t ThreadAffinityMask) const
{
	DWORD ThreadId{ 0 };

	std::cout << "Creating separate hooking thread" << std::endl;
	HANDLE threadHandle = CreateThread(nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE> (Debugger::ModifyIdt),
		reinterpret_cast<PVOID>(const_cast<Debugger*>(this)), CREATE_SUSPENDED, &ThreadId);

	if (!threadHandle)
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "Setting affinity mask to thread" << std::endl;
	if (!SetThreadAffinityMask(threadHandle, static_cast<DWORD_PTR>(ThreadAffinityMask)))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "resuming thread" << std::endl;
	if (-1 == ResumeThread(threadHandle))
	{
		throw ErrorInfo::MakeLastError();
	}

	std::cout << "waiting for the thread termination" << std::endl;
	WaitForSingleObject(threadHandle, INFINITE);
	CloseHandle(threadHandle);
}

void Debugger::ModifyIdt(PVOID objectPointer)
{
	Debugger* currentObject = reinterpret_cast<Debugger*>(objectPointer);
	if (currentObject)
	{
		std::cout << "Calling modification from static method" << std::endl;
		currentObject->HookIdtInCurrentContext();
	}
}

std::string Debugger::GetCurrentPath()
{
	char buffer[MAX_PATH] = { 0 };
	if (!::GetCurrentDirectoryA(sizeof(buffer) - sizeof(buffer[0]), buffer))
	{
		throw ErrorInfo::MakeLastError();
	}
	return std::string(buffer);
}
