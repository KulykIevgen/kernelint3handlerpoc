#pragma once
#include "pch.h"
#include <Windows.h>

#define Debug_device 0x8000 + 100
#define Hook_Idt 0x800 + 100
#define Insert_Break Hook_Idt + 1
#define IOCTL_DEBUGGER_HOOK_IDT CTL_CODE(Debug_device, Hook_Idt, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DEBUGGER_INSERT_BREAKPOINT CTL_CODE(Debug_device, Insert_Break, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define DeviceName "//.////DebugDevice"
#define DriverFileName "BreakHandler.sys"

enum Action
{
	Dump,
	Skip
};

#pragma pack(1)
typedef struct _Breakpoint
{
	DWORD address;
	DWORD processId;
	Action action;
}Breakpoint, *PBreakpoint;
#pragma pack()
