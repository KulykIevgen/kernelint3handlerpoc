﻿#include "pch.h"
#include <iostream>
#include "Debugger.h"

int main()
{
	try 
	{
		std::cout << "Begin execution" << std::endl;
		Debugger debugger;
		auto&& info = debugger.MakeAffinityProcess("AntiDebug.exe");
		std::cout << info.ProcessId << " : " << info.Affinity << " : " << std::endl;
		debugger.HookIdt(info.Affinity);
		auto original_byte = debugger.SetBreak(0x00401FB4, info);
		std::cout << "View original byte: " << std::hex << static_cast<int> (original_byte) << std::dec << std::endl;
		std::cout << "Resuming thread" << std::endl;
		ResumeThread(info.ThreadHandle);
		WaitForSingleObject(info.ProcessHandle, INFINITE);
	}
	catch (const std::exception& error)
	{
		std::cout << error.what() << std::endl;
	}
}
