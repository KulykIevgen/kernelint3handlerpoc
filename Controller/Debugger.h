#pragma once
#include <Windows.h>
#include <string>
#include <memory>
#include "DriverInstaller.h"

struct ProcessInfo
{
	uint32_t ProcessId;
	HANDLE ProcessHandle;
	DWORD_PTR Affinity;
	HANDLE ThreadHandle;
};

class Debugger
{
public:
	Debugger();
	~Debugger() noexcept;
	Debugger(const Debugger&) = delete;
	Debugger(Debugger&&) = delete;
	Debugger& operator=(const Debugger&) = delete;
	Debugger& operator=(Debugger&&) = delete;
	ProcessInfo MakeAffinityProcess(const std::string& ExeFullPath) const noexcept;
	void HookIdt(DWORD_PTR Affinity) const;
	uint8_t SetBreak(uint32_t Address, const ProcessInfo& information) const;
	void HookIdtInCurrentContext() const noexcept;

private:
	void HookIdtForProcessor(uint32_t ThreadAffinityMask) const;
	static void ModifyIdt(PVOID objectPointer);
	static std::string GetCurrentPath();

	HANDLE DebuggerDevice;
	std::unique_ptr<DriverInstaller> Installer;
};

